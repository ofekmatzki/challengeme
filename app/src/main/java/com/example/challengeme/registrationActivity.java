package com.example.challengeme;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.theartofdev.edmodo.cropper.CropImage;

import java.util.HashMap;
import java.util.Map;

public class registrationActivity extends AppCompatActivity {
    public TextView full_name;
    private TextView email;
    private TextView password;
    private TextView auth_password;
    private Button btn_login;
    private FirebaseDb db;
    private ImageView addedImage;
    private Uri imageUri;
    private String myUrl="";
    private StorageTask uploadTask;
    private String str;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        db= new FirebaseDb();
        full_name = (TextView)findViewById(R.id.FullNameData);
        email = (TextView)findViewById(R.id.EmailData);
        password = (TextView)findViewById(R.id.PwdData);
        auth_password = (TextView)findViewById(R.id.PwdDataauth);
        btn_login = (Button)findViewById(R.id.btnLogin);
        addedImage = findViewById(R.id.image_added);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                register_user();
            }
        });
        addedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CropImage.activity().setAspectRatio(1,1).start(registrationActivity.this);
            }
        });


    }
    private String getFileExtension(Uri uri){
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap m = MimeTypeMap.getSingleton();
        return m.getExtensionFromMimeType(contentResolver.getType(uri));
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            imageUri = result.getUri();

            addedImage.setImageURI(imageUri);
        } else {
            Toast.makeText(this, "Something gone wrong!", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(registrationActivity.this, registrationActivity.class));
            finish();
        }
    }
    private void register_user()
    {
        String Message="";
        if(email.getText().toString().isEmpty())Message= "please Enter yo*ur Email";
        if(password.getText().toString().isEmpty())Message= "please Enter your password";
        if(auth_password.getText().toString().isEmpty())Message= "please Enter your password for the second time";
        if(!password.getText().toString().equals(auth_password.getText().toString()))Message= "the passwords don't match";
        if(!Message.isEmpty()){
            Toast.makeText(this,Message,
                    Toast.LENGTH_SHORT).show();
            return;
        }
        if (imageUri != null)
        {


            final StorageReference ref = db.get_child(System.currentTimeMillis() + "." + getFileExtension(imageUri));
            uploadTask = ref.putFile(imageUri);
            uploadTask.continueWithTask(new Continuation() {
                @Override
                public Object then(@NonNull Task task) throws Exception {

                    if (!task.isComplete())
                        return task.getException();
                    return ref.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {

                    if (task.isSuccessful())
                    {
                        Uri download = task.getResult();
                        myUrl = download.toString();

                        final Map user_data = new HashMap();

                        user_data.put("full_name",full_name.getText().toString());
                        user_data.put("email",email.getText().toString());
                        user_data.put("ImageUri",myUrl);

                        db.create_new_user(email.getText().toString(),password.getText().toString(), new db_callback() {
                            public void on_user(String user_id) {
                                if(!user_id.isEmpty())
                                {
                                    user_data.put("id", user_id);
                                    db.save_data("Users",user_id,user_data);
                                    Intent intent = new Intent(registrationActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                }
                                else{
                                    Toast.makeText(registrationActivity.this,"registrationActivity failed",Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                    else
                        Toast.makeText(registrationActivity.this, "Upload new profile image failed", Toast.LENGTH_SHORT).show();

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(registrationActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
            Toast.makeText(registrationActivity.this,"You didn't chose any profile image to upload", Toast.LENGTH_SHORT).show();




    }
}
