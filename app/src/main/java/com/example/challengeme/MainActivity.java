package com.example.challengeme;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.example.challengeme.Fragments.HomeFragment;
import com.example.challengeme.Fragments.ProfileFragment;
import com.example.challengeme.Fragments.SearchFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView bottomNavBar;
    private Fragment selectedFragment = null;
    private FirebaseDb db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db= new FirebaseDb();
        setContentView(R.layout.activity_main);
        bottomNavBar = findViewById(R.id.bottom_navigation);

        bottomNavBar.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new HomeFragment()).commit();
        //Start
        if(!db.is_user())
        {
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);
        }



    }
    private BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    switch (menuItem.getItemId())
                    {
                        case R.id.nav_home:
                            selectedFragment =  new HomeFragment();
                            //((HomeFragment) selectedFragment).relativeLayout.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT));
                            //comment.setVisibility(View.INVISIBLE);
                            //startActivity(new Intent(MainActivity.this, MainActivity.class));
                            break;

                        case R.id.nav_profile:
                            SharedPreferences.Editor editor = getSharedPreferences("prefs",MODE_PRIVATE).edit();
                            editor.putString("profileId", db.get_curr_usr_id());
                            editor.apply();
                            selectedFragment = new ProfileFragment();
                            //comment.setVisibility(View.INVISIBLE);
                            break;

                        case R.id.nav_search:
                            selectedFragment = new SearchFragment();
                            //comment.setVisibility(View.INVISIBLE);
                            break;

                        case R.id.nav_add_challenge:
                            selectedFragment = null;
                            startActivity(new Intent(MainActivity.this, ChallengesActivity.class));
                            //comment.setVisibility(View.INVISIBLE);
                            //finish();
                            break;


                    }
                    if (selectedFragment != null)
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,selectedFragment).commit();

                    return true;
                }
            };


}
