package com.example.challengeme.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.challengeme.Fragments.Challengesdetailsfragment;
import com.example.challengeme.Model.Challenges;
import com.example.challengeme.R;


import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class ChallengeGridAdapter extends RecyclerView.Adapter<ChallengeGridAdapter.ImageViewHolder> {

    private Context mContext;
    private List<Challenges> challengesList;

    public ChallengeGridAdapter(Context context, List<Challenges> challenges){
        mContext = context;
        challengesList = challenges;
    }

    @NonNull
    @Override
    public ChallengeGridAdapter.ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.challengesgrid, parent, false);
        return new ChallengeGridAdapter.ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ChallengeGridAdapter.ImageViewHolder holder, final int position) {

        final Challenges challenge = challengesList.get(position);

        Glide.with(mContext).load(challenge.getChallengeImage()).into(holder.challenge_image);

        holder.challenge_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = mContext.getSharedPreferences("prefs", MODE_PRIVATE).edit();
                editor.putString("challengeId", challenge.getChallengeId());
                editor.apply();

                ((FragmentActivity)mContext).getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new Challengesdetailsfragment()).commit();
            }
        });

    }

    @Override
    public int getItemCount() {
        return challengesList.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder {

        public ImageView challenge_image;


        public ImageViewHolder(View itemView) {
            super(itemView);

            challenge_image = itemView.findViewById(R.id.challenge_image);

        }
    }
}