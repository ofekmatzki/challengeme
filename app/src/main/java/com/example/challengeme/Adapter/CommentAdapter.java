package com.example.challengeme.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.challengeme.CommentActivity;
import com.example.challengeme.EditCommentActivity;
import com.example.challengeme.EditProfileActivity;
import com.example.challengeme.FirebaseDb;
import com.example.challengeme.Fragments.ProfileFragment;
import com.example.challengeme.MainActivity;
import com.example.challengeme.Model.Challenges;
import com.example.challengeme.Model.Comment;
import com.example.challengeme.Model.User;
import com.example.challengeme.R;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.sql.Timestamp;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class CommentAdapter extends  RecyclerView.Adapter<CommentAdapter.ViewHolder>{

    public Context context;
    public List<Comment> commentsList;

    private FirebaseDb Db;
    private FirebaseUser user;


    public CommentAdapter(Context context, List<Comment> comment) {
        this.context = context;
        this.commentsList = comment;
        Db = new FirebaseDb();
    }

    public CommentAdapter() {
        Db = new FirebaseDb();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.comment_item,parent, false);
        return new CommentAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        user = Db.getCurrentUser();

        final Comment comment =commentsList.get(position);
        Glide.with(context).load(comment.getCommentImage()).into(holder.commentImage);
        holder.comment.setText(comment.getDescription());


        getComment(holder, comment.getPublisherId(),comment.getCommentImage());

        if (comment.getPublisherId().equals(Db.get_curr_usr_id())) {
            holder.deleteComment.setVisibility(View.VISIBLE);
            holder.editComment.setVisibility(View.VISIBLE);
        }
        else {
            holder.deleteComment.setVisibility(View.INVISIBLE);
            holder.editComment.setVisibility(View.VISIBLE);
        }

        holder.deleteComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteComment(comment.getCommentId());
            }
        });
        holder.editComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = context.getSharedPreferences("prefs", Context.MODE_PRIVATE).edit();
                editor.putString("commentId", comment.getCommentId());
                editor.apply();
                context.startActivity(new Intent(context, EditCommentActivity.class));

            }
        });


        holder.date.setText(new Timestamp(comment.getDate()).toString());

    }

    @Override
    public int getItemCount() {
        return commentsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public CircleImageView profileImage;
        public TextView comment, username;
        public ImageView commentImage;
        public Button deleteComment, editComment;
        public TextView date;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            profileImage = itemView.findViewById(R.id.image_profile);
            commentImage = itemView.findViewById(R.id.comment_image);
            date = itemView.findViewById(R.id.date);
            username = itemView.findViewById(R.id.username);
            comment = itemView.findViewById(R.id.comment);
            deleteComment = itemView.findViewById(R.id.deleteComment);
            editComment = itemView.findViewById(R.id.editComment);

        }
    }
    private void getComment(final @NonNull ViewHolder holder, final String userid, final String commentImage) {
        DatabaseReference ref = Db.database_reference("Users", userid);

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                //user.setId(userid);
                Glide.with(context).load(user.getImageUri()).into(holder.profileImage);
                Glide.with(context).load(commentImage).into(holder.commentImage);
                holder.username.setText(user.getFull_name());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void deleteComment(final String commentId)
    {
        Db.database_table_reference("Comments",commentId).removeValue();
        Toast.makeText(context, "Successfully deleted!", Toast.LENGTH_SHORT).show();
    }
}
