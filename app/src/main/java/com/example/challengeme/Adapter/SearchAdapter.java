package com.example.challengeme.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.challengeme.FirebaseDb;
import com.example.challengeme.Fragments.ProfileFragment;
import com.example.challengeme.Model.Challenges;
import com.example.challengeme.Model.User;
import com.example.challengeme.R;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class SearchAdapter extends  RecyclerView.Adapter<SearchAdapter.ViewHolder>{

    public Context context;
    public List<User> usersList;

    private FirebaseDb Db;
    private FirebaseUser currentUser;


    public SearchAdapter(Context context, List<User> users) {
        this.context = context;
        this.usersList = users;
        Db = new FirebaseDb();
    }

    public SearchAdapter() {
        Db = new FirebaseDb();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.user_item,parent, false);
        return new SearchAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        currentUser = Db.getCurrentUser();

        final User user = usersList.get(position);

        holder.username.setText(user.getFull_name());
        Glide.with(context).load(user.getImageUri()).into(holder.profileImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = context.getSharedPreferences("prefs", Context.MODE_PRIVATE).edit();
                editor.putString("profileId", user.getId());
                editor.apply();

                ((FragmentActivity) context).getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new ProfileFragment()).commit();
            }
        });

    }

    @Override
    public int getItemCount() {
        return usersList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public CircleImageView profileImage;

        public TextView  username;

        public Button goToProfile;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            profileImage = itemView.findViewById(R.id.image_profile);
            username = itemView.findViewById(R.id.username);






        }
    }

}
