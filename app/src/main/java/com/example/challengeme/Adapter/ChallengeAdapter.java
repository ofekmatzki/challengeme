package com.example.challengeme.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.challengeme.CommentActivity;
import com.example.challengeme.EditChallengeActivity;
import com.example.challengeme.EditCommentActivity;
import com.example.challengeme.FirebaseDb;
import com.example.challengeme.Fragments.Challengesdetailsfragment;
import com.example.challengeme.Fragments.ProfileFragment;
import com.example.challengeme.Model.Challenges;
import com.example.challengeme.Model.Comment;
import com.example.challengeme.Model.User;
import com.example.challengeme.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.sql.Timestamp;
import java.util.List;

public class ChallengeAdapter extends  RecyclerView.Adapter<ChallengeAdapter.ViewHolder>{

    public Context context;
    public List<Challenges> challengesList;

    private FirebaseDb Db;
    private FirebaseUser user;


    public ChallengeAdapter(Context context, List<Challenges> challenge) {
        this.context = context;
        this.challengesList = challenge;
        Db = new FirebaseDb();
    }

    public ChallengeAdapter() {
        Db = new FirebaseDb();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.challenge_item,parent, false);
        return new ChallengeAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        user = Db.getCurrentUser();

        final Challenges challenge =challengesList.get(position);
        Glide.with(context).load(challenge.getChallengeImage()).into(holder.challengeImage);
        if (challenge.getDescription().equals(""))
            holder.description.setVisibility(View.GONE);
        else {
            holder.description.setVisibility(View.VISIBLE);
            holder.description.setText(challenge.getDescription());
        }

        holder.challengeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = context.getSharedPreferences("prefs",Context.MODE_PRIVATE).edit();
                editor.putString("challengeId", challenge.getChallengeId());
                editor.apply();
                ((FragmentActivity)context).getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new Challengesdetailsfragment()).commit();
            }
        });

        holder.profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = context.getSharedPreferences("prefs", Context.MODE_PRIVATE).edit();
                editor.putString("profileId", challenge.getPublisherId());
                editor.apply();

                ((FragmentActivity)context).getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new ProfileFragment()).commit();
            }
        });
        if (challenge.getPublisherId().equals(user.getUid()))
        {
            holder.editChallenge.setVisibility(View.VISIBLE);
            holder.deleteChallenge.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.editChallenge.setVisibility(View.INVISIBLE);
            holder.deleteChallenge.setVisibility(View.INVISIBLE);
        }
        holder.deleteChallenge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteChallenge(challenge.getChallengeId());
            }
        });

        holder.editChallenge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = context.getSharedPreferences("prefs", Context.MODE_PRIVATE).edit();
                editor.putString("challengeId", challenge.getChallengeId());
                editor.apply();
                context.startActivity(new Intent(context, EditChallengeActivity.class));
            }
        });

        holder.addNewComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = context.getSharedPreferences("prefs", Context.MODE_PRIVATE).edit();
                editor.putString("challengeId", challenge.getChallengeId());
                editor.apply();
                context.startActivity(new Intent(context, CommentActivity.class));
            }
        });
        holder.comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = context.getSharedPreferences("prefs", Context.MODE_PRIVATE).edit();
                editor.putString("challengeId", challenge.getChallengeId());
                editor.apply();
                context.startActivity(new Intent(context, CommentActivity.class));
            }
        });

        holder.date.setText(new Timestamp(challenge.getDate()).toString());

        publisherInfo(holder.profileImage, holder.username, holder.publisher, challenge.getPublisherId());

        setCommentsText(challenge.getChallengeId(),holder.comments);
    }

    public void setCommentsText(final String challengeId,final TextView comments)
    {
        DatabaseReference ref = Db.get_reference("Comments");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int i = 0;
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Comment comment = snapshot.getValue(Comment.class);
                    if (comment.getChallengeId().equals(challengeId)){
                        i++;
                    }
                }
                comments.setText("View all "+ i + " comments");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }
    @Override
    public int getItemCount() {
        return challengesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView profileImage, challengeImage;

        public TextView  username, publisher, description, comments;

        public ImageView addNewComment;

        public Button editChallenge, deleteChallenge;

        public TextView date;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            profileImage = itemView.findViewById(R.id.image_profile);
            challengeImage = itemView.findViewById(R.id.challenge_image);
            comments = itemView.findViewById(R.id.comments);

            username = itemView.findViewById(R.id.username);
            publisher = itemView.findViewById(R.id.publisher);
            description = itemView.findViewById(R.id.description);
            editChallenge = itemView.findViewById(R.id.editChallenge);
            deleteChallenge = itemView.findViewById(R.id.deleteChallenge);
            date  = itemView.findViewById(R.id.date);
            addNewComment = itemView.findViewById(R.id.add_new_comment);





        }
    }
    private void publisherInfo(final ImageView image_profile, final TextView username, final TextView publisher, final String userid) {
        DatabaseReference ref = Db.database_reference("Users", userid);

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                //user.setId(userid);
                Glide.with(context).load(user.getImageUri()).into(image_profile);
                username.setText(user.getFull_name());
                publisher.setText(user.getEmail());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
    private void deleteChallenge(final String challengeId)
    {

        final DatabaseReference ref = Db.get_reference("Comments");

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Comment comment = snapshot.getValue(Comment.class);
                    if (comment.getChallengeId().equals(challengeId))
                        Db.database_reference("Comments", comment.getCommentId()).removeValue();

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        Db.database_reference("Challenges", challengeId).removeValue();


    }
}
