package com.example.challengeme.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.challengeme.Adapter.ChallengeAdapter;
import com.example.challengeme.FirebaseDb;
import com.example.challengeme.Model.Challenges;
import com.example.challengeme.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class Challengesdetailsfragment extends Fragment {

    private String challengeId;

    private RecyclerView recyclerView;
    private ChallengeAdapter challengeAdapter;
    private List<Challenges> challengesList;
    private FirebaseDb Db;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_challenges_details, container, false);

        SharedPreferences prefs = getContext().getSharedPreferences("prefs", Context.MODE_PRIVATE);
        challengeId = prefs.getString("challengeId", "none");

        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);

        challengesList = new ArrayList<>();
        challengeAdapter = new ChallengeAdapter(getContext(), challengesList);
        recyclerView.setAdapter(challengeAdapter);

        Db = new FirebaseDb();
        readPost();

        return view;
    }

    private void readPost(){
        DatabaseReference ref = Db.database_table_reference("Challenges", challengeId);

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                challengesList.clear();
                Challenges challenge = dataSnapshot.getValue(Challenges.class);
                challengesList.add(challenge);
                challengeAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


}
