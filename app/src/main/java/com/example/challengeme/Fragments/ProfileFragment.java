package com.example.challengeme.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.challengeme.Adapter.ChallengeGridAdapter;
import com.example.challengeme.EditCommentActivity;
import com.example.challengeme.EditProfileActivity;
import com.example.challengeme.FirebaseDb;
import com.example.challengeme.LoginActivity;
import com.example.challengeme.Model.Challenges;
import com.example.challengeme.Model.Comment;
import com.example.challengeme.Model.User;
import com.example.challengeme.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


public class ProfileFragment extends Fragment {



    private ImageView profileImage;

    private TextView challenges, full_name;

    private Button editProfile, logOut;

    private FirebaseUser currentUser;
    private FirebaseAuth firebaseAuth;
    private FirebaseDb Db;
    private String profileId;

    private RecyclerView recyclerView;
    private ChallengeGridAdapter challengeGridAdapter;
    private List<Challenges> challengesList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        Db = new FirebaseDb();
        currentUser =Db.getCurrentUser();

        SharedPreferences  prefs = getContext().getSharedPreferences("prefs", Context.MODE_PRIVATE);
        profileId = prefs.getString("profileId", "none");
        logOut = view.findViewById(R.id.signOut);

        editProfile= view.findViewById(R.id.edit_profile);
        challenges = view.findViewById(R.id.challenges);
        full_name = view.findViewById(R.id.username);
        profileImage =view.findViewById(R.id.image_profile);
        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new GridLayoutManager(getContext(), 3);
        recyclerView.setLayoutManager(mLayoutManager);
        challengesList = new ArrayList<>();
        challengeGridAdapter = new ChallengeGridAdapter(getContext(), challengesList);
        recyclerView.setAdapter(challengeGridAdapter);

        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getContext(), EditProfileActivity.class));
            }
        });



        FirebaseAuth.AuthStateListener authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() == null) {
                    Toast.makeText(getContext(), "Successfully Logged out!", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getContext(), LoginActivity.class));
                }
            }
        };

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuth.addAuthStateListener(authStateListener);

        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firebaseAuth.signOut();
            }
        });



        userInfo();
        getNumberOfChallenges();
        loadChallenges();

        if (profileId.equals(Db.get_curr_usr_id()))
        {
            editProfile.setText("Edit Profile");
            logOut.setVisibility(View.VISIBLE);
        }
        else
        {
            logOut.setVisibility(View.INVISIBLE);
            editProfile.setVisibility(View.GONE);
        }
        return view;
    }


    private void userInfo(){
        DatabaseReference ref =Db.database_table_reference("Users", profileId);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (getContext() == null){
                    return;
                }
                User user = dataSnapshot.getValue(User.class);
                Glide.with(getContext()).load(user.getImageUri()).into(profileImage);
                full_name.setText(user.getFull_name());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void getNumberOfChallenges(){
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Challenges");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int i = 0;
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Challenges challenge = snapshot.getValue(Challenges.class);
                    if (challenge.getPublisherId().equals(profileId)){
                        i++;
                    }
                }
                challenges.setText(""+i);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private void loadChallenges(){
        DatabaseReference reference = Db.get_reference("Challenges");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                challengesList.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Challenges challenge = snapshot.getValue(Challenges.class);
                    if (challenge.getPublisherId().equals(profileId)){
                        challengesList.add(challenge);
                    }
                }
                Collections.reverse(challengesList);
                challengeGridAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}
