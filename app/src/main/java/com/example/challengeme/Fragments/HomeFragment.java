package com.example.challengeme.Fragments;

import android.content.Context;
import android.net.Uri;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.challengeme.Adapter.ChallengeAdapter;
import com.example.challengeme.FirebaseDb;
import com.example.challengeme.Sqllite_room.sqllite_db;
import com.example.challengeme.Model.Challenges;
import com.example.challengeme.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;


public class HomeFragment extends Fragment {


    private RecyclerView recyclerView;
    private ChallengeAdapter challengeAdapter;
    private List<Challenges> challengesList;
    private FirebaseDb Db;
    private sqllite_db sDB;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        // if connected to Internet save 10 last challenges
        sDB = new sqllite_db(view.getContext().getApplicationContext());
        Db = new FirebaseDb();
        if (is_conntected_to_internet()) {
            save_challenges_to_localDB();
        }
        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);
        challengesList = new ArrayList<>();
        challengeAdapter = new ChallengeAdapter(getContext(), challengesList);
        recyclerView.setAdapter(challengeAdapter);


        readChallenges();
        return view;
    }

    private void readChallenges() {

        if (!is_conntected_to_internet()) {
            try{
                challengesList.clear();
                challengesList.addAll(sDB.load_All_Challenges());
            }
            catch (ExecutionException E){
                Toast.makeText(getContext(), "can't load challenges", Toast.LENGTH_SHORT);
                return;
            }
            catch (InterruptedException E){
                Toast.makeText(getContext(), "can't load challenges", Toast.LENGTH_SHORT);
                return;
            }
            challengeAdapter.notifyDataSetChanged();
        } else {
            Query ref = Db.get_reference("Challenges").orderByChild("date").limitToLast(2);

            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    challengesList.clear();

                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Challenges challenge = snapshot.getValue(Challenges.class);

                        challengesList.add(challenge);

                    }
                    challengeAdapter.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }
        private void save_challenges_to_localDB ()
        {
            Query query = Db.get_reference("Challenges").orderByChild("date").limitToLast(10);
            query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    List<Challenges> temp_challengesList = new ArrayList<>();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Challenges challenge = snapshot.getValue(Challenges.class);
                        temp_challengesList.add(challenge);
                    }
                    sDB.insertChallanges(temp_challengesList);
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }

        private boolean is_conntected_to_internet ()
        {
            ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null &&
                    activeNetwork.isConnectedOrConnecting();
            return isConnected;
        }
    }

