package com.example.challengeme.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.challengeme.Adapter.ChallengeAdapter;
import com.example.challengeme.Adapter.CommentAdapter;
import com.example.challengeme.CommentActivity;
import com.example.challengeme.FirebaseDb;
import com.example.challengeme.Model.Challenges;
import com.example.challengeme.Model.Comment;
import com.example.challengeme.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class CommentFragment extends Fragment {


    private RecyclerView recyclerView;
    private CommentAdapter commentAdapter;
    private List<Comment> commentsList;
    private TextView addNewComment;
    private FirebaseDb Db;
    private String challengeId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_comment, container, false);

        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);
        commentsList = new ArrayList<>();
        commentAdapter = new CommentAdapter(getContext(), commentsList);
        recyclerView.setAdapter(commentAdapter);

        Db = new FirebaseDb();
        SharedPreferences prefs = getContext().getSharedPreferences("prefs", Context.MODE_PRIVATE);
        challengeId = prefs.getString("challengeId", "none");
        addNewComment = view.findViewById(R.id.add_comment);

        addNewComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), CommentActivity.class));
            }
        });

        readComments();
        return view;
    }

    private void readComments(){
        DatabaseReference ref = Db.get_reference("Comments");

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                commentsList.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Comment comment = snapshot.getValue(Comment.class);
                    if (comment.getChallengeId().equals(challengeId))
                        commentsList.add(comment);

                }
                commentAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


}
