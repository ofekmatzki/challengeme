package com.example.challengeme;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    TextView email;
    TextView password;
    Button btn_login;
    TextView register_link;
    FirebaseDb db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        db= new FirebaseDb();
        email = (TextView)findViewById(R.id.EmailData);
        password = (TextView)findViewById(R.id.PwdData);
        btn_login = (Button)findViewById(R.id.btnLogin);
        register_link = (TextView)findViewById(R.id.lnkRegister);
        register_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, registrationActivity.class);
                startActivity(intent);
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login(email.getText().toString(),password.getText().toString());
            }
        });
    }
    protected void login(String email,String password)
    {
        String Message="";
        if(email.isEmpty())Message= "please Enter your Email";
        if(password.isEmpty())Message= "please Enter your password";
        if(!Message.isEmpty()){
            Toast.makeText(this,Message,
                    Toast.LENGTH_SHORT).show();
            return;
        }
        db.login_user(email,password, new db_callback() {
            public void on_user(String user_id) {
                if(!user_id.isEmpty())
                {
                    Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                    startActivity(intent);
                }
                else{
                    Toast.makeText(LoginActivity.this,"login failed",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
