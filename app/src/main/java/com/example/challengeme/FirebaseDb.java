package com.example.challengeme;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.Map;

public class FirebaseDb {
    private FirebaseAuth mAuth;
    public FirebaseDb()
    {
        mAuth = FirebaseAuth.getInstance();
    }

    boolean is_user()
    {
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null)
        {
            return true;
        }
        return false;
    }
    public FirebaseUser getCurrentUser()
    {
        return mAuth.getCurrentUser();
    }
    public String get_curr_usr_id()
    {
        return mAuth.getCurrentUser().getUid();
    }
    void create_new_user(String email, String password, final db_callback callback)
    {
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    callback.on_user(get_curr_usr_id());
                } else {
                    callback.on_user("");
                }
            }
        });
    }
    void login_user(String email, String password, final db_callback callback)
    {
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    callback.on_user(get_curr_usr_id());
                } else {
                    callback.on_user("");
                }
            }
        });
    }
    void save_data(String table_name, String user_id, Map user_data_map)
    {
        DatabaseReference curr_user_db= FirebaseDatabase.getInstance().getReference().child(table_name).child(user_id);
        curr_user_db.setValue(user_data_map);
    }
    public String get_key(String table_name)
    {
        DatabaseReference DBref = FirebaseDatabase.getInstance().getReference(table_name);

        return DBref.push().getKey();
    }
    public StorageReference get_child(String str)
    {
        return FirebaseStorage.getInstance().getReference().child(str);
    }
    public DatabaseReference database_reference(String table, String uid)
    {
        return FirebaseDatabase.getInstance().getReference().child(table).child(uid);
    }
    public DatabaseReference get_reference(String table)
    {
        return FirebaseDatabase.getInstance().getReference(table);
    }
    public StorageReference get_storage_reference(String table)
    {
        return FirebaseStorage.getInstance().getReference(table);
    }
    public DatabaseReference database_table_reference(String table, String key)
    {
        return FirebaseDatabase.getInstance().getReference(table).child(key);
    }
}
