package com.example.challengeme;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.theartofdev.edmodo.cropper.CropImage;

import android.net.Uri;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class ChallengesActivity extends AppCompatActivity {

    private Uri imageUri;
    private String myUrl="";
    private StorageTask uploadTask;

    private FirebaseDb Db;

    private ImageView close, addedImage;
    private TextView save;
    private EditText description;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenges);

        Db = new FirebaseDb();

        close = findViewById(R.id.close);
        addedImage = findViewById(R.id.image_added);
        save = findViewById(R.id.save);
        description = findViewById(R.id.description);


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ChallengesActivity.this, MainActivity.class));
                finish();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveImage();
            }
        });

        addedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CropImage.activity().setAspectRatio(1,1).start(ChallengesActivity.this);
            }
        });

    }


    private String getFileExtension(Uri uri){
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap m = MimeTypeMap.getSingleton();
        return m.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            imageUri = result.getUri();

            addedImage.setImageURI(imageUri);
        } else {
            Toast.makeText(this, "Something gone wrong!", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(ChallengesActivity.this, MainActivity.class));
            finish();
        }
    }

    private void saveImage()
    {
        final ProgressDialog progress = new ProgressDialog(this);
        progress.setMessage("Saving Challenge");

        progress.show();

        if (imageUri != null)
        {


            final StorageReference ref = Db.get_child(System.currentTimeMillis() + "." + getFileExtension(imageUri));
            uploadTask = ref.putFile(imageUri);
            uploadTask.continueWithTask(new Continuation() {
                @Override
                public Object then(@NonNull Task task) throws Exception {

                    if (!task.isComplete())
                        return task.getException();
                    return ref.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {

                    if (task.isSuccessful())
                    {
                        Uri download = task.getResult();
                        myUrl = download.toString();


                        //DatabaseReference DBref = FirebaseDatabase.getInstance().getReference("Challenges");

                        String challengeId = Db.get_key("Challenges");
                        String publisherId = Db.get_curr_usr_id();

                        Date date = new Date();


                        HashMap<String,Object> data = new HashMap<>();
                        data.put("challengeId",challengeId);
                        data.put("publisherId",publisherId);
                        data.put("challengeImage",myUrl);
                        data.put("description", description.getText().toString());
                        data.put("date", date.getTime());

                        Db.save_data("Challenges", challengeId, data);

                        progress.dismiss();

                        startActivity(new Intent(ChallengesActivity.this, MainActivity.class));
                        finish();

                    }
                    else
                        Toast.makeText(ChallengesActivity.this, "Upload new challenge failed", Toast.LENGTH_SHORT).show();

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                     Toast.makeText(ChallengesActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
            Toast.makeText(ChallengesActivity.this,"You didn't chose any challenge to upload", Toast.LENGTH_SHORT).show();




    }
}
