package com.example.challengeme.Sqllite_room;
import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import com.example.challengeme.Model.Challenges;

@Database(entities = {Challenges.class}, version = 1)
public abstract class AppDb extends RoomDatabase {
        private static AppDb instance;
        public abstract ChallangeDao challangeDao();

        public static synchronized AppDb getInstance(Context context)
        {
                if(instance==null)
                {
                        instance= Room.databaseBuilder(context.getApplicationContext(),
                                AppDb.class,"Sqlitedb").fallbackToDestructiveMigration().build();
                }
                return instance;
        }
}
