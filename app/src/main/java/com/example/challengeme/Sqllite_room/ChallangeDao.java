package com.example.challengeme.Sqllite_room;

import android.icu.text.Replaceable;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.challengeme.Model.Challenges;

import java.util.List;

@Dao
public interface ChallangeDao {
    @Query("SELECT * FROM Challenges")
    List<Challenges> getAll();

    @Query("SELECT * FROM Challenges WHERE challengeId = :challengeId")
    Challenges loadById(String challengeId );

    @Query("SELECT * FROM Challenges WHERE challengeId IN (:challengeIds)")
    List<Challenges> loadAllByIds(String[] challengeIds);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Challenges ch);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Challenges... challenges);

    @Delete
    void delete(Challenges ch);
}