package com.example.challengeme.Sqllite_room;

import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;

import androidx.room.Room;


import com.example.challengeme.Model.Challenges;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class sqllite_db {
    private ChallangeDao challangeDao;

    public sqllite_db(Context context)
    {
        AppDb database = AppDb.getInstance(context);
        challangeDao= database.challangeDao();
    }

    public List<Challenges> load_Challanges(String[] challengeIds){
        List<Challenges> challenges = challangeDao.loadAllByIds(challengeIds);
        return challenges;
    }
//    public List<Challenges> load_All_Challenges(){
//        List<Challenges> challenges = challangeDao.getAll();
//        return challenges;
//    }
    public List<Challenges> load_All_Challenges() throws ExecutionException, InterruptedException {

        Callable<List<Challenges>> callable = new Callable<List<Challenges>>() {
            @Override
            public List<Challenges> call() throws Exception {
                return challangeDao.getAll();
            }
        };
        Future<List<Challenges>> future = Executors.newSingleThreadExecutor().submit(callable);
        return future.get();
    }
    public Void insertChallanges(List<Challenges> chlst){
        new InsertChallengesAsyncTask(challangeDao).execute(chlst.toArray(new Challenges[chlst.size()]));
        return null;
    }
    private static class InsertChallengesAsyncTask extends AsyncTask<Challenges,Void,Void>{
        private  ChallangeDao challangeDao;
        private InsertChallengesAsyncTask(ChallangeDao chDao){
            challangeDao=chDao;
        }
        @Override
        protected Void doInBackground(Challenges... challenges) {
            challangeDao.insertAll(challenges);
            return null;
        }
    }
}
