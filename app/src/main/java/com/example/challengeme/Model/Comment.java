package com.example.challengeme.Model;

import java.sql.Timestamp;

public class Comment {



    private String commentId;
    private String publisherId;
    private String challengeId;
    private String commentImage;
    private String description;
    private Long date;


    public Comment(String commentId, String publisherId, String challengeid, String commentImage, String description, Long date) {
        this.commentId = commentId;
        this.publisherId = publisherId;
        this.commentImage = commentImage;
        this.description = description;
        this.challengeId=challengeid;
        this.date = date;
    }

    public Comment() {

    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public String getChallengeId() {
        return challengeId;
    }

    public void setChallengeId(String challengeId) {
        this.challengeId = challengeId;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(String publisherId) {
        this.publisherId = publisherId;
    }

    public String getCommentImage() {
        return commentImage;
    }

    public void setCommentImage(String commentImage) {
        this.commentImage = commentImage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
