package com.example.challengeme.Model;
import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class Challenges {
    @PrimaryKey @NonNull
    private String challengeId;
    private String publisherId;
    private String challengeImage;
    private String description;
    private Long date;

    public Challenges(String challengeId, String challengeImage, Long date, String description, String publisherId) {
        this.challengeId = challengeId;
        this.publisherId = publisherId;
        this.challengeImage = challengeImage;
        this.description = description;
        this.date = date;
    }

    @Ignore
    public Challenges() {
    }

    public String getChallengeId() {
        return challengeId;
    }

    public String getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(String publisherId) {
        this.publisherId = publisherId;
    }

    public String getChallengeImage() {
        return challengeImage;
    }

    public void setChallengeImage(String challengeImage) {
        this.challengeImage = challengeImage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setChallengeId(String challengeId) {
        this.challengeId = challengeId;

    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }
}
