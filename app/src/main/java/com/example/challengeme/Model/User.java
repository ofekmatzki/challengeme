package com.example.challengeme.Model;

public class User {
    private String ImageUri;
    private String email;
    private String full_name;
    private String id;


    public User(String imageUri , String email , String full_name , String id) {
        this.email = email;
        this.full_name = full_name;
        ImageUri = imageUri;
        this.id = id;
    }

    public User() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getImageUri() {
        return ImageUri;
    }

    public void setImageUri(String imageUri) {
        ImageUri = imageUri;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}