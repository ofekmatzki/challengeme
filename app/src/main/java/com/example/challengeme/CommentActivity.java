package com.example.challengeme;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.challengeme.Adapter.CommentAdapter;
import com.example.challengeme.Model.Comment;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.theartofdev.edmodo.cropper.CropImage;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class CommentActivity extends AppCompatActivity {

    private Uri imageUri;
    private String myUrl="";
    private StorageTask uploadTask;

    private FirebaseDb Db;

    private ImageView close, addedImage;
    private TextView save;
    private EditText description;

    private RecyclerView recyclerView;
    private CommentAdapter commentAdapter;
    private List<Comment> commentsList;
    private TextView addNewComment;
    private String challengeId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_comment);

        Db = new FirebaseDb();


        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);
        commentsList = new ArrayList<>();
        commentAdapter = new CommentAdapter(this, commentsList);
        recyclerView.setAdapter(commentAdapter);

        Db = new FirebaseDb();
        SharedPreferences prefs = getSharedPreferences("prefs", Context.MODE_PRIVATE);
        challengeId = prefs.getString("challengeId", "none");
        addNewComment = findViewById(R.id.add_comment);

        addNewComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(R.layout.activity_comment);
                close = findViewById(R.id.close);
                addedImage = findViewById(R.id.image_added);
                save = findViewById(R.id.save);
                description = findViewById(R.id.description);


                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(CommentActivity.this, MainActivity.class));
                        finish();
                    }
                });

                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        saveImage();
                    }
                });

                addedImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CropImage.activity().setAspectRatio(1,1).start(CommentActivity.this);
                    }
                });
            }
        });

        readComments();


    }

    private void readComments(){
        DatabaseReference ref = Db.get_reference("Comments");

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                commentsList.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Comment comment = snapshot.getValue(Comment.class);
                    if (comment.getChallengeId().equals(challengeId))
                        commentsList.add(comment);

                }
                commentAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private String getFileExtension(Uri uri){
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap m = MimeTypeMap.getSingleton();
        return m.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            imageUri = result.getUri();

            addedImage.setImageURI(imageUri);
        } else {
            Toast.makeText(this, "Something gone wrong!", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(CommentActivity.this, CommentActivity.class));
            finish();
        }
    }

    private void saveImage()
    {
        final ProgressDialog progress = new ProgressDialog(this);
        progress.setMessage("Saving Comment");

        progress.show();

        if (imageUri != null)
        {


            final StorageReference ref = Db.get_child(System.currentTimeMillis() + "." + getFileExtension(imageUri));
            uploadTask = ref.putFile(imageUri);
            uploadTask.continueWithTask(new Continuation() {
                @Override
                public Object then(@NonNull Task task) throws Exception {

                    if (!task.isComplete())
                        return task.getException();
                    return ref.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {

                    if (task.isSuccessful())
                    {
                        Uri download = task.getResult();
                        myUrl = download.toString();




                        String commentId = Db.get_key("Comments");
                        String publisherId = Db.get_curr_usr_id();
                        Date date = new Date();



                        HashMap<String,Object> data = new HashMap<>();
                        data.put("commentId",commentId);
                        data.put("publisherId",publisherId);
                        data.put("challengeId",challengeId);
                        data.put("commentImage",myUrl);
                        data.put("description", description.getText().toString());
                        data.put("date",date.getTime());

                        Db.save_data("Comments", commentId, data);

                        progress.dismiss();

                        startActivity(new Intent(CommentActivity.this, CommentActivity.class));
                        finish();

                    }
                    else
                        Toast.makeText(CommentActivity.this, "Upload new comment failed", Toast.LENGTH_SHORT).show();

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(CommentActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
            Toast.makeText(CommentActivity.this,"You didn't chose any comment to upload", Toast.LENGTH_SHORT).show();




    }
}
